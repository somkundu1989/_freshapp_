<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('localization/{locale}', function ($locale) {
	\Session::put('locale', $locale);
	return redirect()->back();
});

Route::get('/clear-all', function () {
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('route:clear');
    Artisan::call('view:clear');
    exec('composer dump-autoload');
});

Route::group(['middleware' => 'auth'], function () {

	Route::get('/', 'HomeController@index')->name('home');
	Route::resources([
	    'employees' => 'EmployeeController'
	]);

	Route::resources([
	    'companies' => 'CompanyController'
	]);
});