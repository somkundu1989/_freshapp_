@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ trans('employees.show') }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('employees.destroy', $employee->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('employees.index') }}" class="btn btn-primary" title="{{ trans('employees.show_all') }}">
                        <span class="fa fa-list-ul" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('employees.create') }}" class="btn btn-success" title="{{ trans('employees.create') }}">
                        <span class="fa fa-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('employees.edit', $employee->id ) }}" class="btn btn-primary" title="{{ trans('employees.edit') }}">
                        <span class="fa fa-edit" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="{{ trans('employees.delete') }}" onclick="return confirm(&quot;{{ trans('employees.confirm_delete') }}?&quot;)">
                        <span class="fa fa-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>{{ trans('employees.first_name') }}</dt>
            <dd>{{ $employee->first_name }}</dd>

            <dt>{{ trans('employees.last_name') }}</dt>
            <dd>{{ $employee->last_name }}</dd>

            <dt>{{ trans('employees.email') }}</dt>
            <dd>{{ $employee->email }}</dd>

            <dt>{{ trans('employees.phone') }}</dt>
            <dd>{{ $employee->phone }}</dd>

            <dt>{{ trans('employees.company_id') }}</dt>
            <dd>{{ $employee->getcompany->name }}-{!! $employee->getcompany->email !!}</dd>

        </dl>

    </div>
</div>

@endsection