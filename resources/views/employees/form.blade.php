
<div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
    <label for="first_name" class="col-md-2 control-label">{{ trans('employees.first_name') }}</label>
    <div class="col-md-10">
        <input class="form-control" name="first_name" type="text" id="first_name" value="{{ old('first_name', optional($employee)->first_name) }}" minlength="1" maxlength="255" placeholder="{{ trans('employees.first_name__placeholder') }}">
        {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
    <label for="last_name" class="col-md-2 control-label">{{ trans('employees.last_name') }}</label>
    <div class="col-md-10">
        <input class="form-control" name="last_name" type="text" id="last_name" value="{{ old('last_name', optional($employee)->last_name) }}" minlength="1" maxlength="255" placeholder="{{ trans('employees.last_name__placeholder') }}">
        {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
    <label for="email" class="col-md-2 control-label">{{ trans('employees.email') }}</label>
    <div class="col-md-10">
        <input class="form-control" name="email" type="email" id="email" value="{{ old('email', optional($employee)->email) }}" minlength="1" maxlength="255" placeholder="{{ trans('employees.email__placeholder') }}">
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
    <label for="phone" class="col-md-2 control-label">{{ trans('employees.phone') }}</label>
    <div class="col-md-10">
        <input class="form-control" name="phone" type="text" id="phone" value="{{ old('phone', optional($employee)->phone) }}" minlength="1" maxlength="255" placeholder="{{ trans('employees.phone__placeholder') }}">
        {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('company_id') ? 'has-error' : '' }}">
    <label for="company_id" class="col-md-2 control-label">{{ trans('employees.company_id') }}</label>
    <div class="col-md-10">
        <select class="form-control" name="company_id" id="company_id">
            <option value="0">{{ trans('employees.company_id__placeholder') }}</option>
            @foreach($companies as $company)
                <option value="{!! $company->id !!}" {!! (old('company_id', optional($employee)->company_id)==$company->id) ? 'selected="selected"' : '' !!}>{!! $company->name !!} ({!! $company->email !!})</option>
            @endforeach
        </select>
        {!! $errors->first('company_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>



