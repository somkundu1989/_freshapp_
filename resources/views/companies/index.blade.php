@extends('layouts.app')

@section('content')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
        
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="mt-5 mb-5">{{ trans('companies.model_plural') }}</h4>
            </div>

            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('companies.create') }}" class="btn btn-success" title="{{ trans('companies.create') }}">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                </a>
            </div>

        </div>
        
        @if(count($companies) == 0)
            <div class="panel-body text-center">
                <h4>{{ trans('companies.none_available') }}</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>{{ trans('companies.name') }}</th>
                            <th>{{ trans('companies.logo') }}</th>
                            <th>{{ trans('companies.email') }}</th>
                            <th>{{ trans('companies.website') }}</th>

                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($companies as $company)
                        <tr>
                            <td>{{ $company->name }}</td>
                            <td><img src="{{ asset('logo/'.$company->logo) }}" width="50" /></td>
                            <td>{{ $company->email }}</td>
                            <td>{{ $company->website }}</td>

                            <td>

                                <form method="company" action="{!! route('companies.destroy', $company->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-xs pull-right" role="group">
                                        <a href="{{ route('companies.show', $company->id ) }}" class="btn btn-info" title="{{ trans('companies.show') }}">
                                            <span class="fa fa-eye" aria-hidden="true"></span>
                                        </a>
                                        <a href="{{ route('companies.edit', $company->id ) }}" class="btn btn-primary" title="{{ trans('companies.edit') }}">
                                            <span class="fa fa-edit" aria-hidden="true"></span>
                                        </a>

                                        <button type="submit" class="btn btn-danger" title="{{ trans('companies.delete') }}" >
                                            <span class="fa fa-trash" aria-hidden="true"></span>
                                        </button>
                                    </div>

                                </form>
                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $companies->render() !!}
        </div>
        
        @endif
    
    </div>
@endsection