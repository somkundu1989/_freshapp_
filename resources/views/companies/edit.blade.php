@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
  
        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="mt-5 mb-5">{{ trans('companies.edit') }}</h4>
            </div>
            <div class="btn-group btn-group-sm pull-right" role="group">

                <a href="{{ route('companies.index') }}" class="btn btn-primary" title="{{ trans('companies.show_all') }}">
                    <i class="fa fa-list-ul" aria-hidden="true"></i>
                </a>

                <a href="{{ route('companies.create') }}" class="btn btn-success" title="{{ trans('companies.create') }}">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                </a>

            </div>
        </div>

        <div class="panel-body">

            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form method="POST" action="{{ route('companies.update', $company->id) }}" id="edit_company_form" name="edit_company_form" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="PUT">
            @include ('companies.form', [
                                        'company' => $company,
                                      ])

                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <input class="btn btn-primary" type="submit" value="{{ trans('companies.update') }}">
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection