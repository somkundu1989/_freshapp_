<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\_dev\MailController;
use App\Http\Requests\CompanyStoreRequest;
use App\Model\Company;
use Carbon\Carbon;
use Exception;
use File;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::paginate(10);

        return view('companies.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyStoreRequest $request)
    {
        try {

            $data = $request->validated();

            $file     = $request->logo;
            $fileName = md5(Carbon::now()) . $file->getClientOriginalName();
            $file->move(storage_path('app/public/logo/'), $fileName);

            Company::create(array_merge($data, ['logo' => $fileName]));
            $emailDataArr = [
                'title'   => "A new company is been created",
                'content' => 'Hello, <br>A new company is been created with this email account.',

                'subject' => "A new company is been created",
                'from'    => [
                    'email' => 'info@testtool.ok',
                    'name'  => 'Som- GoodDev',
                ],
                'to'      => [
                    'email' => $data['email'],
                    'name'  => $data['name'],
                ],
                'view'    => 'manage.email',
            ];

            MailController::firemail($emailDataArr);

            return redirect()->route('companies.index')
                ->with('success_message', trans('companies.model_was_added'));

        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => trans('companies.unexpected_error')]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::findOrFail($id);

        return view('companies.show', compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::findOrFail($id);

        return view('companies.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyStoreRequest $request, $id)
    {
        try {

            $data = $request->validated();

            $company = Company::findOrFail($id);
            $logoArr = [];
            if (!empty($request->logo)) {

                $file     = $request->logo;
                $fileName = md5(Carbon::now()) . $file->getClientOriginalName();
                $file->move(storage_path('app/public/logo/'), $fileName);
                $logoArr = ['logo' => $fileName];

                if ($company && $company->logo) {

                    File::delete(storage_path('app/public/logo/' . $company->logo));
                }
            }

            $company->update(array_merge($data, $logoArr));
            $emailDataArr = [
                'title'   => "Company is been updated",
                'content' => 'Hello, <br>Company is been updated with this email account.',

                'subject' => "Company is been updated",
                'from'    => [
                    'email' => 'info@testtool.ok',
                    'name'  => 'Som- GoodDev',
                ],
                'to'      => [
                    'email' => $data['email'],
                    'name'  => $data['name'],
                ],
                'view'    => 'manage.email',
            ];

            MailController::firemail($emailDataArr);

            return redirect()->route('companies.index')
                ->with('success_message', trans('companies.model_was_updated'));

        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => trans('companies.unexpected_error')]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $company = Company::findOrFail($id);
            $company->delete();

            return redirect()->route('companies.index')
                ->with('success_message', trans('companies.model_was_deleted'));

        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => trans('companies.unexpected_error')]);
        }
    }
}
