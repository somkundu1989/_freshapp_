<?php

use Illuminate\Database\Seeder;
use App\Model\User;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        User::create([
            
                'name' => 'Admin',
                'email' => 'admin@admin.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$E5wjK9a26E3B2Hxgs24QdeoG1HffKqs/I0NEU0WQ/Bi69Ke8I9THy',
                'remember_token' => '9nALa95iWk0JKgdBkk4qWObkUiowBGGfkSE7KksthJ83eoD4tSXj8naCPOCx',
                'created_at' => '2018-11-13 13:11:21',
                'updated_at' => '2018-11-13 13:11:21',
            ]
        );
        
        
    }
}