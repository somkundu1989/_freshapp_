
      <ul class="app-menu">
        <li><a class="app-menu__item active" href="{!! route('home') !!}"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">@lang('dashboard.title')</span></a></li>

        <li><a class="app-menu__item active" href="{!! route('companies.index') !!}"><i class="app-menu__icon fa fa-th-list"></i><span class="app-menu__label">@lang('companies.title')</span></a></li>

        <li><a class="app-menu__item active" href="{!! route('employees.index') !!}"><i class="app-menu__icon fa fa-th-list"></i><span class="app-menu__label">@lang('employees.title')</span></a></li>
      </ul>