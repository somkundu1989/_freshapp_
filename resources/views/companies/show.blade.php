@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ trans('companies.show') }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('companies.destroy', $company->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('companies.index') }}" class="btn btn-primary" title="{{ trans('companies.show_all') }}">
                        <span class="fa fa-list-ul" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('companies.create') }}" class="btn btn-success" title="{{ trans('companies.create') }}">
                        <span class="fa fa-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('companies.edit', $company->id ) }}" class="btn btn-primary" title="{{ trans('companies.edit') }}">
                        <span class="fa fa-edit" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="{{ trans('companies.delete') }}" onclick="return confirm(&quot;{{ trans('companies.confirm_delete') }}?&quot;)">
                        <span class="fa fa-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>{{ trans('companies.name') }}</dt>
            <dd>{{ $company->name }}</dd>

            <dt>{{ trans('companies.logo') }}</dt>
            <dd>{{ $company->logo }}</dd>

            <dt>{{ trans('companies.email') }}</dt>
            <dd>{{ $company->email }}</dd>

            <dt>{{ trans('companies.website') }}</dt>
            <dd>{{ $company->website }}</dd>

        </dl>

    </div>
</div>

@endsection