<?php

return [
    'id'                      => 'Id',
    'id__placeholder'         => 'Enter id here...',

    'first_name'              => 'First Name',
    'first_name__placeholder' => 'Enter first name here...',

    'last_name'               => 'Last Name',
    'last_name__placeholder'  => 'Enter last_name here...',

    'email'                   => 'E-mail',
    'email__placeholder'      => 'Enter email here...',

    'phone'                   => 'Phone',
    'phone__placeholder'      => 'Enter phone here...',

    'company_id'              => 'Company',
    'company_id__placeholder' => 'Choose the company here...',
    'company_id__request__min'  => 'Please choose a company.',

    'create'                  => 'Create New Employee',
    'delete'                  => 'Delete Employee',
    'edit'                    => 'Edit Employee',
    'show'                    => 'Show Employee',
    'show_all'                => 'Show All Employees',
    'add'                     => 'Add',
    'update'                  => 'Update',
    'confirm_delete'          => 'Delete Employee?',
    'none_available'          => 'No Employees Available!',
    'title'                   => 'Employees',
    'model_plural'            => 'Employees',
    'model_was_added'         => 'Employee was successfully added!',
    'model_was_updated'       => 'Employee was successfully updated!',
    'model_was_deleted'       => 'Employee was successfully deleted!',
    'unexpected_error'        => 'Unexpected error occurred while trying to process your request!',

];
