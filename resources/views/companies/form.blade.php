
<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    <label for="name" class="col-md-2 control-label">{{ trans('companies.name') }}</label>
    <div class="col-md-10">
        <input class="form-control" name="name" type="text" id="name" value="{{ old('name', optional($company)->name) }}" minlength="1" maxlength="255" placeholder="{{ trans('companies.name__placeholder') }}">
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
    <label for="email" class="col-md-2 control-label">{{ trans('companies.email') }}</label>
    <div class="col-md-10">
        <input class="form-control" name="email" type="email" id="email" value="{{ old('email', optional($company)->email) }}" minlength="1" maxlength="255" placeholder="{{ trans('companies.email__placeholder') }}">
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('logo') ? 'has-error' : '' }}">
    <label for="logo" class="col-md-2 control-label">{{ trans('companies.logo') }}</label>
    <div class="col-md-10">
        @if(optional($company)->logo)
            <img src="{{ asset('logo/'.optional($company)->logo) }}" width="50" />
            <br>
        @endif
        <input class="form-control" name="logo" type="file" id="logo" value="{{ old('logo', optional($company)->logo) }}" minlength="1" maxlength="255" placeholder="{{ trans('companies.logo__placeholder') }}">
        {!! $errors->first('logo', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('website') ? 'has-error' : '' }}">
    <label for="website" class="col-md-2 control-label">{{ trans('companies.website') }}</label>
    <div class="col-md-10">
        <input class="form-control" name="website" type="text" id="website" value="{{ old('website', optional($company)->website) }}" minlength="1" maxlength="255" placeholder="{{ trans('companies.website__placeholder') }}">
        {!! $errors->first('website', '<p class="help-block">:message</p>') !!}
    </div>
</div>



