<?php

namespace App\Http\Controllers\_dev;

use App\Http\Controllers\Controller;
use Mail;

class MailController extends Controller
{

    public function initmail()
    {
        $emailDataArr = [
            'title'   => "Test it from me",
            'content' => "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
            'subject' => "A very good subject",
            'from'    => [
                'email' => 'somenathk@capitalnumbers.com',
                'name'  => 'Som- Capital Numbers',
            ],
            'to'      => [
                'email' => 'official.som@gmail.com',
                'name'  => 'Official Som',
            ],
            'view'    => '_dev.manageMail.firemail',
        ];
        $this->firemail($emailDataArr);
    }

    public static function firemail($data)
    {

        Mail::send($data['view'], ['title' => $data['title'], 'content' => $data['content']], function ($message) use ($data) {

            $message->from($data['from']['email'], $data['from']['name']);
            $message->to($data['to']['email'], $data['to']['name']);
            $message->subject($data['subject']);

        });
    }
}
