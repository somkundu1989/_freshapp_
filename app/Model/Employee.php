<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = [
        'first_name', 'last_name', 'email', 'phone', 'company_id',
    ];
    
    public function getCompany()
    {
        return $this->belongsTo('App\Model\Company', 'company_id');
    }
}
