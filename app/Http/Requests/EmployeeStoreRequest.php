<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|min:1|max:255',
            'last_name' => 'required|string|min:1|max:255',
            'email' => 'string|min:1|max:255|email',
            'phone' => 'digits_between:0,9999999999',
            'company_id' => 'required|numeric|min:1',
        ];
    }

    /**
     * @return array
     *
     * Override default error messages.
     */

    public function messages()
    {
        return [

            'company_id.min' => trans('employees.company_id__request__min'),
        ];
    }
}
