<!DOCTYPE html>
<html lang="{!! app()->getLocale() !!}">
  <head>
    <title>{{ config('app.name') }}</title>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="{!! asset('_admin/css/main.css') !!}">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body>
    <header class="app-header">
      <ul class="app-nav">
        
        <!-- language Menu-->
        <li class="dropdown">
          <a class="btn btn-primary dropdown-toggle app-nav__item" type="button" data-toggle="dropdown">{!! App::getLocale()=='en'?'English':'French'  !!}
          <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="{!! url('localization/en') !!}">English</a></li>
            <li><a class="dropdown-item" href="{!! url('localization/fr') !!}">French</a></li>
          </ul>
        </li>
        <!-- User Menu-->
        @guest        
        <li class="dropdown">
          <a class="app-nav__item" href="{{ route('login') }}"><i class="fa fa-user fa-lg"></i> &nbsp; {{ __('Login') }}</a>
        </li>
        <li class="dropdown">
          @if (Route::has('register'))
            <a class="app-nav__item" href="{{ route('register') }}"><i class="fa fa-user fa-lg"></i> &nbsp; {{ __('Register') }}</a>
          @endif
        </li>
        @else
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i> &nbsp; {{ Auth::user()->name }}</a>
          <ul class="dropdown-menu settings-menu dropdown-menu-right">
            <li><a class="dropdown-item" href="#"><i class="fa fa-cog fa-lg"></i> Settings</a></li>
            <li><a class="dropdown-item" href="#"><i class="fa fa-user fa-lg"></i> Profile</a></li>
            <li>
              <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out fa-lg"></i> {{ __('Logout') }}</a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
              </form>
            </li>
          </ul>
        </li>
        @endguest
        
      </ul>
    </header>
    <section class="material-half-bg">
      <div class="cover"></div>
    </section>
    <section class="login-content">
      <div class="logo">
        <h1>{{ config('app.name') }}</h1>
      </div>

        @yield('content')
    </section>
    <!-- Essential javascripts for application to work-->
    <script src="{!! asset('_admin/js/jquery-3.2.1.min.js') !!}"></script>
    <script src="{!! asset('_admin/js/popper.min.js') !!}"></script>
    <script src="{!! asset('_admin/js/bootstrap.min.js') !!}"></script>
    <script src="{!! asset('_admin/js/main.js') !!}"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="{!! asset('_admin/js/plugins/pace.min.js') !!}"></script>
    <script type="text/javascript">
      // Login Page Flipbox control
      $('.login-content [data-toggle="flip"]').click(function() {
        $('.login-box').toggleClass('flipped');
        return false;
      });
    </script>
  </body>
</html>