<?php

return [
    'id'                   => 'Id',
    'id__placeholder'      => 'Enter id here...',

    'name'                 => 'Company Name',
    'name__placeholder'    => 'Enter company name here...',

    'email'                => 'E-mail',
    'email__placeholder'   => 'Enter email here...',

    'logo'                 => 'Company Logo',
    'logo__placeholder'    => 'Enter logo here...',

    'website'              => 'Company Website',
    'website__placeholder' => 'Enter company website here...',

    'create'               => 'Create New Company',
    'delete'               => 'Delete Company',
    'edit'                 => 'Edit Company',
    'show'                 => 'Show Company',
    'show_all'             => 'Show All Companies',
    'add'                  => 'Add',
    'update'               => 'Update',
    'confirm_delete'       => 'Delete Company?',
    'none_available'       => 'No Companies Available!',
    'title'                => 'Companies',
    'model_plural'         => 'Companies',
    'model_was_added'      => 'Company was successfully added!',
    'model_was_updated'    => 'Company was successfully updated!',
    'model_was_deleted'    => 'Company was successfully deleted!',
    'unexpected_error'     => 'Unexpected error occurred while trying to process your request!',

];
